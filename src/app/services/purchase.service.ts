import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { CommonApiService } from './common-api.service';

@Injectable({
  providedIn: 'root'
})
export class PurchaseService {
  private apiServiceUrl = environment.apiServeUrl

  constructor(private commonApi: CommonApiService) { }

  getPurchase(params?: any) {
		return this.commonApi.get('purchase', params);
	}

	getQuotation(params?: any) {
		return this.commonApi.get('purchase/pq', params);
	}

	getOrder(params?: any) {
		return this.commonApi.get('purchase/po', params);
	}

  insertPurchase(params: any) {
		return this.commonApi.post('purchase', params);
	}

  updatePurchase(params: any) {
		return this.commonApi.put('purchase/' + params.noPq, params);
	}

  deletePurchase(id: string) {
		return this.commonApi.delete('purchase/' + id);
	}
}
