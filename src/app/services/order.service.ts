import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonApiService } from './common-api.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private commonApi: CommonApiService) { }

  getOrder(params?: any) {
		return this.commonApi.get('order', params);
	}

  insertOrder(params: any) {
		return this.commonApi.post('order', params);
	}

  updateOrder(params: any) {
		return this.commonApi.put('order/' + params.id, params);
	}

  deleteOrder(id: number) {
		return this.commonApi.delete('order/' + id);
	}
}
