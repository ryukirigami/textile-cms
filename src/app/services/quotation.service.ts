import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonApiService } from './common-api.service';

@Injectable({
  providedIn: 'root'
})
export class QuotationService {

  constructor(private commonApi: CommonApiService) { }

  getQuotation(params?: any) {
		return this.commonApi.get('quotation', params);
	}

  insertQuotation(params: any) {
		return this.commonApi.post('quotation', params);
	}

  updateQuotation(params: any) {
		return this.commonApi.put('quotation/' + params.id, params);
	}

  deleteQuotation(id: number) {
		return this.commonApi.delete('quotation/' + id);
	}
}
