import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CustomerComponent } from './menu/master/customer/customer.component';
import { OrderComponent } from './menu/reception/order/order.component';
import { QuotationComponent } from './menu/reception/quotation/quotation.component';
import { TemplatesComponent } from './menu/templates/templates.component';
import { AppLayoutComponent } from './app-layout/app-layout.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReportComponent } from './menu/reception/report/report.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomerComponent,
    OrderComponent,
    QuotationComponent,
    TemplatesComponent,
    AppLayoutComponent,
    ReportComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
