export interface Customer {
    id: string
    nama: string
    alamat: string
    telp: string
    ktp: string
    npwp: string
}