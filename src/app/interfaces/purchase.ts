import { Customer } from "./customer";

export interface Purchase {
    noPq: string
    noPo: string
    warna: string
    jenisBahan: string
    jumlah: string
    harga: string
    status: string
    customer: any
    customerName: string
}