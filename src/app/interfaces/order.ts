export interface Order {
    noPo: string
    custId: string
    warna: string
    jenisBahan: string
    jumlah: string
    harga: string
    status: string
}