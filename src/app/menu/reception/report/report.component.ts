import { Component, ViewChild, ElementRef } from '@angular/core';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
// import 'jspdf-autotable';


@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent {

  @ViewChild('htmlData', {static: false}) htmlData!: ElementRef;


  head = [['No PO', 'Customer Id', 'Warna', 'Jenis Bahan', 'Jumlah', 'Harga', 'Status']]
  data = [
    ['123451', 'PT. ABC 1', 'Merah', 'Poliester', '20', '120.000', 'Selesai'],
    ['123452', 'PT. ABC 2', 'Merah', 'Poliester', '20', '120.000', 'Selesai'],
    ['123453', 'PT. ABC 3', 'Merah', 'Poliester', '20', '120.000', 'Selesai'],
    ['123454', 'PT. ABC 4', 'Merah', 'Poliester', '20', '120.000', 'Selesai'],
    ['123455', 'PT. ABC 5', 'Merah', 'Poliester', '20', '120.000', 'Selesai'],
    ['123456', 'PT. ABC 6', 'Merah', 'Poliester', '20', '120.000', 'Selesai']
  ]


  constructor() { 
  }

  printPdf() {
    var doc = new jsPDF();

    doc.setFontSize(18);
    doc.text('Invoice Report', 11, 8);
    doc.setFontSize(11);
    doc.setTextColor(100);


    (doc as any).autoTable({
      head: this.head,
      body: this.data,
      theme: 'plain',
      didDrawCell: (data: { column: { index: any; }; }) => {
        console.log(data.column.index)
      }
    })
    doc.save('report.pdf');
  } 

  // public printPdf():void {
  //   const DATA = document.getElementById('htmlData')!;
      
  //   html2canvas(DATA).then(canvas => {
   
  //       let fileWidth = 208;
  //       let fileHeight = canvas.height * fileWidth / canvas.width;
        
  //       const FILEURI = canvas.toDataURL('image/png')
  //       let PDF = new jsPDF('p', 'mm', 'a4');
  //       let position = 0;
  //       PDF.addImage(FILEURI, 'PNG', 0, position, fileWidth, fileHeight)
        
  //       PDF.save('report.pdf');
  //   });     
  // }

}