import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { Purchase } from 'src/app/interfaces/purchase';
import { PurchaseService } from 'src/app/services/purchase.service';
import { CustomerService } from 'src/app/services/customer.service';
import { Customer } from 'src/app/interfaces/customer';
import { Router } from '@angular/router';
// import { AddNewQuotationComponent } from './add-new-quotation/add-new-quotation.component';
// import { DeleteQuotationComponent } from './delete-quotation/delete-quotation.component';
// import { EditQuotationComponent } from './edit-quotation/edit-quotation.component';

@Component({
  selector: 'app-quotation',
  templateUrl: './quotation.component.html',
  styleUrls: ['./quotation.component.scss']
})
export class QuotationComponent implements OnInit {

  purchase: Purchase = this.resetPurchase();
  purchases: Purchase[] | undefined;
  inputData: string = '';

  constructor(private customerService: CustomerService, private purchaseService: PurchaseService, private router: Router) { }

  ngOnInit(): void {
    this.getData();
    this.resetForm();
  }

  getData(quotation?: any) {
    this.purchaseService.getQuotation().subscribe(
      (response: Purchase[]) => {
        console.log(response);
        this.purchases = response;
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );
  }

  insertData(quotation: Purchase) {
    quotation.customer = {id: quotation.customer}
    this.purchaseService.insertPurchase(quotation).subscribe(
      (response: Purchase[]) => {
        console.log(response);
        this.getData();
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );
  }

  createOrder(order: Purchase) {
    order.status = "ON_ORDER";
    order.warna = this.purchase.warna;
    order.jenisBahan = this.purchase.jenisBahan;
    order.jumlah = this.purchase.jumlah;
    order.customer = this.purchase.customer;
    order.noPo = order.noPq.replace('PQ', 'PO');

    console.log(order, this.purchase);
    this.purchaseService.updatePurchase(order).subscribe(
      (response: Purchase[]) => {
        // console.log(response);
        // this.getData();
        this.router.navigateByUrl('/order');
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
        console.log("nomor quotation sudah digunakan!");
      }
    );
  }

  editData(quotation: Purchase) {
    console.log(quotation);
    this.purchase = quotation;
  }

  updateData(quotation: Purchase) {
    quotation.status = this.purchase.status;
    quotation.customer = this.purchase.customer;
    this.purchaseService.updatePurchase(quotation).subscribe(
      (response: Purchase[]) => {
        console.log(response);
        this.getData();
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );
  }

  deleteData(id: string) {
    this.purchaseService.deletePurchase(id).subscribe(
      (response: Purchase[]) => {
        console.log(response);
        this.getData();
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );
  }

  resetForm(): void {
    this.purchase = this.resetPurchase();
  }

  resetPurchase(): Purchase {
    return {
      noPq: '',
      noPo: '',
      customer: '',
      warna: '',
      jenisBahan: '',
      jumlah: '',
      harga: '',
      status: '',
      customerName: ''
    }
  }
}
