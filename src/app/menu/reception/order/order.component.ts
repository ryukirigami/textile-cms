import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Purchase } from 'src/app/interfaces/purchase';
import { PurchaseService } from 'src/app/services/purchase.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  purchase: Purchase = this.resetPurchase();
  purchases: Purchase[] | undefined;

  constructor(private purchaseService: PurchaseService) { }

  ngOnInit(): void {
    this.getData();
    this.resetForm();
  }

  getData(quotation?: any) {
    this.purchaseService.getOrder().subscribe(
      (response: Purchase[]) => {
        console.log(response);
        this.purchases = response;
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );
  }

  insertData(order: Purchase) {}

  editData(order: Purchase) {
    console.log(order);
    this.purchase = order;
  }

  updateData(order: Purchase) {
    order.customer = this.purchase.customer;
    order.status = this.purchase.status;
    console.log(order, this.purchase);
    this.purchaseService.updatePurchase(order).subscribe(
      (response: Purchase[]) => {
        console.log(response);
        this.getData();
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );
  }

  deleteData(id: string) {
    this.purchaseService.deletePurchase(id).subscribe(
      (response: Purchase[]) => {
        console.log(response);
        this.getData();
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );
  }

  resetForm(): void {
    this.purchase = this.resetPurchase();
  }

  resetPurchase(): Purchase {
    return {
      noPq: '',
      noPo: '',
      customer: '',
      warna: '',
      jenisBahan: '',
      jumlah: '',
      harga: '',
      status: '',
      customerName: ''
    }
  }
}
