import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Customer } from 'src/app/interfaces/customer';
import { Purchase } from 'src/app/interfaces/purchase';
import { CustomerService } from 'src/app/services/customer.service';
import { PurchaseService } from 'src/app/services/purchase.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {
  customer: Customer = this.resetCustomer();
  customers: Customer[] | undefined;
  inputData: string = '';
  searchName: string = '';

  purchase: Purchase = this.resetPurchase();

  constructor(private customerService: CustomerService, private purchaseService: PurchaseService, private router: Router) { }

  ngOnInit(): void {
    this.getData();
    this.resetForm();
  }

  onSearchChange(ev: any) {
    console.log(ev);
    this.customerService.getCustomer({nama: ev}).subscribe(
      (response: Customer[]) => {
        console.log(response);
        this.customers = response;
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );
  }

  getData(customer?: any) {
    this.customerService.getCustomer().subscribe(
      (response: Customer[]) => {
        console.log(response);
        this.customers = response;
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );
  }

  addQuotation(customer: Customer) {
    console.log(customer);
    this.purchase.customer = customer.id;
    this.purchase.customerName = customer.nama;
  }

  insertQuotation(purchase: Purchase) {
    console.log(purchase);
    this.purchaseService.insertPurchase(purchase).subscribe(
      (response: Purchase[]) => {
        // console.log(response);
        // this.getData();
        this.router.navigateByUrl('/quotation');
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
        alert("nomor quotation sudah digunakan!");
      }
    );
  }

  insertData(customer: Customer) {
    this.customerService.insertCustomer(customer).subscribe(
      (response: Customer[]) => {
        console.log(response);
        this.getData();
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );
  }

  editData(customer: Customer) {
    console.log(customer);
    this.customer = customer;
  }

  updateData(customer: Customer) {
    this.customerService.updateCustomer(customer).subscribe(
      (response: Customer[]) => {
        console.log(response);
        this.getData();
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );
  }

  deleteData(id: number) {
    this.customerService.deleteCustomer(id).subscribe(
      (response) => {
        console.log(response);
        this.getData();
        if(response.conflict) {
          setTimeout(() => alert('Customer has related data with purchases!'), 200);
        }
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );
  }

  resetForm(): void {
    this.customer = this.resetCustomer();
  }

  resetCustomer(): Customer {
    return {
      id: '',
      nama: '',
      alamat: '',
      ktp: '',
      telp: '',
      npwp: ''
    }
  }

  resetPurchase(): Purchase {
    return {
      noPq: '',
      noPo: '',
      customer: '',
      warna: '',
      jenisBahan: '',
      jumlah: '',
      harga: '',
      status: '',
      customerName: ''
    }
  }
}
